'use strict';

let $canvas = document.querySelector('canvas');
let context = $canvas.getContext('2d');

let stick = {
  width: 300,
  height: 10,
  posX: $canvas.width - 300,
  color: '#bababa'
};

let randIn = (min, max) => Math.floor(Math.random() * (max - min));

let antId = 0;
function Ant(posX = 0, posY = 0) {
  let isGoingLeft = Math.floor(Math.random() * 2) === 0;

  this.id = antId++;
  this.posX = posX;
  this.posY = posY;
  this.width = 10;
  this.height = 5;
  this.color = `rgb(${randIn(0, 255)}, ${randIn(0, 255)}, ${randIn(0, 255)})`;
  this.direction = isGoingLeft ? 'left' : 'right';
  this.speed = 3;
  this.isDead = false;

  this.kill = () => {
    this.isDead = true;
    this.color = 'red';
  }
};

function drawStick() {
  context.beginPath();
  context.moveTo(0, $canvas.height / 1.2);
  context.strokeStyle = stick.color;
  context.lineTo($canvas.width, $canvas.height / 1.2);
  context.stroke();
}

function createAnts(antsCount) {
  let arr = [];
  do {
    arr.push(antsCount);
    antsCount--;
  } while(antsCount > 0);

  return arr.map(i => new Ant(
    -40 + i * ($canvas.width / arr.length),
    $canvas.height / 1.2 - 5)
  );
}

function drawAnts(ants) {
  ants.filter(ant => !ant.isDead).forEach(ant => {
    ants.forEach(otherAnt => {
      if(ant.id === otherAnt.id) { return; }

      if(ant.direction === 'right' &&
         otherAnt.direction === 'left' &&
         ant.posX + ant.width >= otherAnt.posX &&
         ant.posX + ant.width < otherAnt.posX + otherAnt.width
        ) {
         ant.direction = 'left';
         otherAnt.direction = 'right';
        }
    });

    if(ant.posX >= $canvas.width || ant.posX <= -ant.width) {
      ant.kill();
    }

    switch(ant.direction) {
      case 'right':
        ant.posX += ant.speed / 1.5;
        break;
      default:
        ant.posX -= ant.speed / 1.5;
    }

    context.beginPath();
    context.rect(ant.posX, ant.posY, ant.width, ant.height);
    context.fillStyle = ant.color;
    context.fill();
  });
}

function checkIfAllDead(ants) {
  return ants.every(ant => ant.isDead);
}

let ants = createAnts(10);

let lastTime,
    startTime = +new Date();

function render(now) {
  if(!lastTime) { lastTime = now; }
  let elapsed = now - lastTime;

  if(elapsed > 1000 / 30) {
    context.clearRect(0, 0, $canvas.width, $canvas.height);

    drawStick();
    drawAnts(ants);

    lastTime = now;
  }

  let timePassed = new Date(+new Date() - startTime);
  if(!checkIfAllDead(ants)) {
    window.requestAnimationFrame(render);
    document.getElementById('all-dead').innerText = `${timePassed.getMinutes()}m ${timePassed.getSeconds()}s.`;
  } else {
    document.getElementById('all-dead').innerText = `all dead after ${timePassed.getMinutes()}m ${timePassed.getSeconds()}s.`;
  }
}

window.requestAnimationFrame(render);

