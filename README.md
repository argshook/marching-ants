# Marching Ants

a random set of ants put at random location are marching towards the end.

conditions:

* all ants have same speed
* all ants die once goes over the edge
* if ant bumps into another ant, both ants switch walkin direction

question:

how long would it take for all ants to reach edge?

## install

* npm i
(some global npms might be needed, most probably babel-core)

## play

* `npm start`
* `python -m SimpleHTTPServer` [localhost:8000](http://localhost:8000)

## tests

* `npm test` (if you have any :))

